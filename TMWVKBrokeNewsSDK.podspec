

Pod::Spec.new do |s|

  s.name         = "TMWVKBrokeNewsSDK"
  s.version      = "0.0.3"
  s.summary      = "TM web"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/TMWVKBrokeNewsSpec.git", :tag => s.version}

  s.source_files = 'TMFramework/TMWVKBrokeNewsSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/TMWVKBrokeNewsSDK.framework'
  s.requires_arc = true
  s.dependency "TMSDK"
  s.dependency "TMUserCenter"
s.dependency "TMWebViewKitSDK"


end
